# Partimos de la imagen raiz de node
FROM node

# Carpeta raiz o directorio de trabajo
WORKDIR /techu-bpjjsa-front

# Dependencias globales
RUN npm install -g polymer-cli --unsafe-perm
RUN npm install -g bower --unsafe-perm

# Copia archivos de la carpeta local a la imagen en la carpeta /proyecto-bpjjsa
# incluye los archivos ocultos
ADD . /techu-bpjjsa-front

# Descarga las dependencias a traves del package json
RUN npm install
RUN bower install --allow-root

# puerto que expone
EXPOSE 8081

# comando inicializacion
# el cmd se ejecuta cuando haya lanzado la imagen
CMD ["polymer", "serve --npm"]